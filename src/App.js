//Base Imports
import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';

//install bootstrap & react bootstrap: bootstrap@4.6.0 react-bootstrap@1.5.
import 'bootstrap/dist/css/bootstrap.min.css';
//css dependency
import './index.css';

//app imports
import { UserProvider } from './userContext.js';
import './App.css';

//App Components
//every component has to be imported before it can be rendered inside index.js; universal to all pages if its in all the pages.
import AppNavbar from './components/AppNavbar.js';


//Page Components
import Home from './pages/Home.js';
import Products from './pages/Products.js';
import ProductView from './pages/ProductView.js';
import Register from './pages/Register.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import PageNotFound from './pages/PageNotFound.js';



function App() {

  // State hook for the user state that's defined here for a global scope
  // Initialized as an object with properties from the localStorage
  // This will be used to store the user information and will be used for validating if a user is logged in on the app or not
    const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

    // Function for clearing localStorage on logout
    const unsetUser = () => {

    localStorage.clear();

    setUser({
      id: null,
      isAdmin: null
    });

    };

    //Because our user state's values are reset to null every time the user reloads the page (thus logging the user out), we want to use React's useEffect hook to fetch the logged-in user's details when the page is reloaded. By using the token saved in localStorage when a user logs in, we can fetch the their data from the database, and re-set the user state values back to the user's details.
    useEffect(() => {

    // console.log(user);

    fetch(`https://protected-brushlands-54138.herokuapp.com/cap/users/details`, {
      headers: {
        Authorization: `Bearer ${ localStorage.getItem('token') }`
      }
    })
    .then(res => res.json())
    .then(data => {

      // Set the user states values with the user details upon successful login.
      if (typeof data._id !== "undefined") {

        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        });

      // Else set the user states to the initial values
      } else {

        setUser({
          id: null,
          isAdmin: null
        });

      }

    })

    }, []);

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Container>
          <Switch>
            <Route exact path="/" component={Home}/>
            <Route exact path="/products" component={Products}/>
            <Route exact path="/ProductView/:productId" component={ProductView}/>
            <Route exact path="/login" component={Login}/>
            <Route exact path="/logout" component={Logout}/>
            <Route exact path="/register" component={Register}/>
            <Route component={Error} />
          </Switch>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
