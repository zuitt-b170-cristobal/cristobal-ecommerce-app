import { Fragment, useEffect, useState, useContext } from 'react';
import AdminView from '../components/AdminView';
import UserView from '../components/UserView';
import userContext from '../userContext';

export default function Products() {

    const { user } = useContext(userContext);

    // State that will be used to store the products retrieved from the database
    const [products, setProducts] = useState([]);

    //Function to fetch our products data. The reason we have this in a function instead of directly in a useEffect hook is so that it can be reused and invoked ONLY when a page needs to re-render, instead of constantly (which causes a memory leak due to infinite looping)
    const fetchData = () => {

        fetch(`https://protected-brushlands-54138.herokuapp.com/cap/products/`)
        .then(res => res.json())
        .then(data => {

            setProducts(data);

        });

    }

    // Retrieves the products from the database upon initial render of the "Products" component
    useEffect(() => {
        fetchData();
    }, []);
    
    return (
        <Fragment>
            {/* If the user is an admin, show the Admin View. If not, show the regular products page. */}
            {(user.isAdmin === true)
                ? <AdminView productsData={products} fetchData={fetchData}/>
                : <UserView productsData={products}/>
            }
        </Fragment>
    )

}
