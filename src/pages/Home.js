import { Fragment } from 'react';


// App Components
import Banner from '../components/Banner.js';
import Highlights from '../components/Highlights.js';

export default function Home() {

	const data = {
	    title: "Play World",
	    content: "Indoor play center for kids and kids at heart.",
	    destination: "/products",
	    label: "Book now!"
	}

	return (
		<Fragment>
			<Banner data={data}/>
			<Highlights />
		</Fragment>
	)
}
