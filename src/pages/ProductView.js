/**
import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useHistory, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import userContext from '../userContext';

export default function ProductView() {

	// The "useParams" hook allows us to retrieve any parameter or the productId passed via the URL
	const { productId } = useParams();
	const { user } = useContext(userContext);


	const history = useHistory();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	Swal.fire({text: "I'm here" & {productId}})


	const orderItem = (productId) => {


		fetch(`https://protected-brushlands-54138.herokuapp.com/cap/products/${ productId }`)
		.then(res => res.json())
		.then(data => {

            console.log(data);

			if (data === true) {

				Swal.fire({
					title: "Successfully booked",
					icon: 'success',
					text: "You have successfully booked this ticket."
				});

				// The "push" method allows us to redirect the user to a different page and is an easier approach rather than using the "Redirect" component
				history.push("/products");

			} else {

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});

			}

        });

	};

	useEffect(() => {


		console.log(productId);

		fetch(`https://protected-brushlands-54138.herokuapp.com/cap/products/${ productId }`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);

		});

	}, [productId]);

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							{ user.id !== null ? 
									<Button variant="primary" block onClick={() => orderItem(productId)}>Book</Button>
								: 
									<Link className="btn btn-danger btn-block" to="/login">Log in to Book</Link>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}
**/



import { Fragment, useEffect, useState, useContext } from 'react';
import AdminView from '../components/AdminView';
import UserView from '../components/UserView';
import userContext from '../userContext';
import { useParams, Link } from 'react-router-dom';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';



export default function ProductView() {

    const { user } = useContext(userContext);
   	const { productId } = useParams();

   	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);


    // State that will be used to store the products retrieved from the database
    const [products, setProducts] = useState([]);

    //Function to fetch our products data. The reason we have this in a function instead of directly in a useEffect hook is so that it can be reused and invoked ONLY when a page needs to re-render, instead of constantly (which causes a memory leak due to infinite looping)
    const fetchData = () => {

        fetch(`https://protected-brushlands-54138.herokuapp.com/cap/products/${ productId }`)
        .then(res => res.json())
        .then(data => {

        	setName(data.getname);
        	setDescription(data.description);
        	setPrice(data.price);


        });

    }

    // Retrieves the products from the database upon initial render of the "Products" component
    useEffect(() => {
        fetchData();
    }, []);
    
    return (
        <Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							{ user.id !== null ? 
									<Button variant="primary" block onClick={() => fetchData(productId)}>Book</Button>
								: 
									<Link className="btn btn-danger btn-block" to="/login">Log in to Book</Link>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
    )

}