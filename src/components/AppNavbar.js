import React, { Fragment, useContext } from 'react';
import {Link, NavLink} from 'react-router-dom';

//Bootstrap Components
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

//app imports
import userContext from '../userContext.js'


export default function AppNavbar(){

  const { user } = useContext(userContext);

  return(
    <Navbar bg='light' expand='lg'>
     <Navbar.Brand as={Link} to='/'>Play World</Navbar.Brand>
     <Navbar.Toggle aria-controls="basic-navbar-nav"/>
     <Navbar.Collapse id="basic-navbar-nav">
       <Nav className="ml-auto">
         <Nav.Link as={NavLink} to='/'>Home</Nav.Link>
         <Nav.Link as={NavLink} to='/products'>Product Catalog</Nav.Link>
         {(user.id !== null) ? 
                            <Nav.Link as={NavLink} to="/logout" exact>Logout</Nav.Link>
                        : 
                            <Fragment>
                                <Nav.Link as={NavLink} to="/login" exact>Login</Nav.Link>
                                <Nav.Link as={NavLink} to="/register" exact>Register</Nav.Link>
                            </Fragment>
                    }
       </Nav>
     </Navbar.Collapse>
    </Navbar>
    )
}