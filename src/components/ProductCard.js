// import { useState, useEffect } from 'react';
// import Card from 'react-bootstrap/Card';
// import Button from 'react-bootstrap/Button';
import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ProductCard({productProp}) {

    // Check to see if the data was properly passed from the "Product" component/page
    // console.log(props);
    // Every component recieves information in the form of an object
    // console.log(typeof props);

    // Deconstruct the course properties into their own variables
    const {_id, name, description, price} = productProp;

  
    return (
        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP {price}</Card.Text>
                <Link className="btn btn-primary" to={`/ProductView/${_id}`}>Details</Link>
            </Card.Body>
        </Card>
    )
}

