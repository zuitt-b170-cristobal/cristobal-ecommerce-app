//dependencies
import React from 'react';

//bootstrap components
import Card from 'react-bootstrap/Card';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

export default function Highlights(){
	return(
		  <Row>
		{/*xs & md - allows setting of breakpoints*/}
		  	<Col xs={12} md={4}>
		  		<Card className="card-highlight">
		  			<Card.Body>
		  				<Card.Title>
		  					<h2>Get the ultimate playground experience</h2>
		  				</Card.Title>
		  				<Card.Text>
		  				Treat your kids for a day of fun as they slip and slide, and jump and bounce around the massive world of fun, excitement and adventure!
		  				</Card.Text>
		  			</Card.Body>
		  		</Card>
		  	</Col>

		  	<Col xs={12} md={4}>
		  		<Card className="card-highlight">
		  			<Card.Body>
		  				<Card.Title>
		  					<h2>Sanitation is a must</h2>
		  				</Card.Title>
		  				<Card.Text>
		  				We have introduced enhanced cleaning measures throughout the day to regularly clean and sanitize the attraction including high-frequency touchpoints and play areas.
		  				</Card.Text>
		  			</Card.Body>
		  		</Card>
		  	</Col>

		  	<Col xs={12} md={4}>
		  		<Card className="card-highlight">
		  			<Card.Body>
		  				<Card.Title>
		  					<h2>Celebrate with us</h2>
		  				</Card.Title>
		  				<Card.Text>
		  				We offer a customisable Birthday Party Package for parties of up to 30 children! School and group bookings can be arranged too.
		  				</Card.Text>
		  			</Card.Body>
		  		</Card>
		  	</Col>
		  </Row>
		)
}