import { Fragment, useState, useEffect } from 'react'
import ProductCard from "./ProductCard";

export default function UserView({productsData}) {

    // console.log(productData);

    const [products, setProducts] = useState([]);

    useEffect(() => {
        
        // Map through the products received from the parent component (products page) to render the product cards
        const productsArr = productsData.map(product => {
            // Returns active products as "ProductCard" components
        	if(product.isActive === true){
				return (
					<ProductCard productProp={product} key={product._id}/>
				)
        	}else{
        		return null;
        	}
        });

        // Set the "products" state with the product card components returned by the map method
        // Allows the product card components to be rendered in this "UserView" component via the return statement below
        setProducts(productsArr);

    }, [productsData]);

    return(
        <Fragment>
            {products}
        </Fragment>
    );
}
